(ns game-of-life.core
  (:gen-class))

(defn -main [])

(defn drop-self [window]
  (concat (subvec window 0 4) (subvec window 5 9)))

(defn count-neighbors [neighbors]
  (count (filter #{1} neighbors)))

(defn survives? [window]
  (and (= 1 (nth window 4)) (= 2 (count-neighbors (drop-self window)))))

(defn isborn? [window]
  (= 3 (count-neighbors (drop-self window))))

(defn lives? [window]
  (if (or (survives? window) (isborn? window))
    true
    false))

(defn should-die? [neighbors]
  (if (or (> 2 (count-neighbors neighbors)) (< 3 (count-neighbors neighbors)))
    true
    false))

(defn calculate-next [window]
  (cond
   (should-die? (drop-self window)) 0
   (lives? window) 1
   :else 0))

(calculate-next [0 0 0 0 1 1 0 0 0])
