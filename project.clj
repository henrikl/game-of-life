(defproject game-of-life "0.1.0-SNAPSHOT"
  :description "Coding Dojo: Game of life"
  :url "http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]]
  :main ^:skip-aot game-of-life.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
