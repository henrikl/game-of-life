(ns game-of-life.core-test
  (:require [clojure.test :refer :all]
            [game-of-life.core :refer :all]))

(deftest returns-something
  (testing "calling the calculate-next method passes"
    (is (= 0 (calculate-next [0 0 0 0 0 0 0 0 0])))))

(deftest stays-dead
  (testing "a bit should stay dead if it has 2 neighbors"
    (is (= 0 (calculate-next [1 0 0 0 0 0 1 0 0])))))

(deftest survives
  (testing "a bit should survive if it is alive and has 2-3 neighbors"
    (is (= 1 (calculate-next [1 0 0 0 1 0 1 0 0])))))

(deftest dies-because-of-lack-of-neighbors
  (testing "a bit should die if it has 0-1 neighbors"
    (is (= 0 (calculate-next [0 0 0 0 1 0 1 0 0])))))

(deftest dies-because-of-too-many-neighbors
  (testing "a bit should die if it has over 3 neighbors"
    (is (= 0 (calculate-next [1 0 1 0 1 0 1 0 1])))))

(deftest is-born
  (testing "a dead bit is (re)born if it has exactly 3 neighbors"
    (is (= 1 (calculate-next [0 1 0 1 0 0 0 0 1])))))

(run-tests)
